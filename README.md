# Penne
Ppartial Python compiler written in OCaml. It compiles Python programs to LLVM IR, which can be interpreted or converted to machine code. With LLVM optimisations it can achieve the speed of C or C++ for running Python programs.

## Limitations
This isn't as flexible as CPython. It supports only a small subset of Python functionality. It was created as a toy project to pass the time of my doomscrolling-self.

### Supported Features:
- Functions with multiple arguments
- Basic array initialization and element access (no modification)
- Conditional if-else blocks
- While loops
- Python-style indentation
- Arithmetic operations

### Important but Not Yet Implemented:
- Type system (currently limited to signed 32-bit integers)
- Classes
- Dynamic lists
- List comprehensions
- For loops
- Sets/Hashtables

## Comparisons
The following comparisons show the execution time in seconds for different programs:

## Fibonacci of 44 (Recursive)
```
def fib(n):
  if n <= 2:
    return 1
  else:
    return fib(n - 1) + fib(n - 2)

print(fib(44))
```
## Python Interpreter: 77.15 seconds
## OCaml_to_LLVM compiler: 1.60 seconds

## Greatest Common Denominator (Iterative)
```
def gcd(a, b):
  while a != b:
      if a > b:
          a = a - b
      else:
          b = b - a
  return a

print(gcd(2147483648, 2))
````
## Python Interpreter: 44.92 seconds
## OCaml_to_LLVM compiler: 0.845 seconds